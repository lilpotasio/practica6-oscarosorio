package programa;

import java.util.Scanner;

import clases.GestorPedidos;

public class DonerKebab {

	public static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
		
		//creo variables
		int select = 0;
		int dia;
		int juego;
		//1- Crear instancia de GestorClases. 
		GestorPedidos gestor= new GestorPedidos();
		//Texto de inicio del programa
		System.out.println("______ _   _                   _   __     _           _     \r\n" + 
				"|  _  (_) (_)                 | | / /    | |         | |    \r\n" + 
				"| | | | ___  _ __   ___ _ __  | |/ /  ___| |__   __ _| |__  \r\n" + 
				"| | | |/ _ \\| '_ \\ / _ \\ '__| |    \\ / _ \\ '_ \\ / _` | '_ \\ \r\n" + 
				"| |/ /| (_) | | | |  __/ |    | |\\  \\  __/ |_) | (_| | |_) |\r\n" + 
				"|___/  \\___/|_| |_|\\___|_|    \\_| \\_/\\___|_.__/ \\__,_|_.__/ \r\n" + 
				"                                                            ");
		do {
			//genero el menu en un metodo
			menu();
            
            select=input.nextInt();
            switch (select) {
			case 1:
				gestor.altaCliente("Mariano", "029847289", "BadBunny", "32");
				gestor.altaProducto("1", "M", "no", "patatas");
				gestor.altaProductoEnsalada("2", "L", "no", "no", "si", "ternera", "si", "si", "si");
				gestor.altaProductoKebab("3", "XL", "si", "si", "si", "mixto", "no", "si");
				break;
			case 2:
				gestor.buscarCliente("029847289");
				gestor.buscarProducto("1");
				gestor.buscarProductoEnsalada("2");
				gestor.buscarProductoKebab("3");
				break;
			case 3:
				gestor.listarCliente();
				break;
			case 4:
				gestor.listarProducto();
				break;
			case 5:
				gestor.listarProductoKebab();
				break;
			case 6:
				gestor.listarProductoEnsalada();
				break;
			case 7:
				
				System.out.println("Qu� dia es hoy?");
				dia = input.nextInt();
				gestor.menuEspecialEnsalada(gestor.buscarProductoEnsalada("2"), dia);
				
				break;
			case 8:
				System.out.println("Qu� dia es hoy?");
				dia = input.nextInt();
				gestor.menuEspecialKebab(gestor.buscarProductoKebab("3"), dia);
				break;
			case 9:
				System.out.println("BIENVENIDO AL JUEGO DEL KEBAB EN EL CUAL PODRAS GANAR MARAVILLOSOS PREMIOS");
				System.out.println("A CONTINUACION ESCRIBE 1 SI QUIERES GANAR UN PREMIO SEGURO O 2 SI TE LA QUIERES"
						+ "JUGAR A TODO O NADA");
				juego = input.nextInt();
				gestor.juego(juego);
				break;
			case 10:
				gestor.eliminarCliente("029847289");
				gestor.eliminarProducto("1");
				gestor.eliminarProductoEnsalada("2");
				gestor.eliminarProductoKebab("3");
				break;
			default:
				break;
			}
		} while (select!=11);
		

	}
	public static void menu() {
		System.out.println("************************************************************************************");
        System.out.println("               Menu                                                                *");
        System.out.println("1.- Dar de alta un cliente, un producto, un producto kebab y un producto ensalada  *");
        System.out.println("2.- Buscar uno de cada                                                             *");
        System.out.println("3.- listar clientes                                                                *");
        System.out.println("4.- listar productos                                                               *");
        System.out.println("5.- listar productos kebab                                                         *");
        System.out.println("6.- Listar productos ensalada                                                      *");
        System.out.println("7.- Metodo extra exclusivo de producto ensalada                                    *");
        System.out.println("8.- Metodo extra exclusivo de producto kebab                                       *");
        System.out.println("9.- Metodo extra gestor JUEGO                                                      *");
        System.out.println("10.- Eliminar cliente, producto, producto kebab, producto ensalada                 *");
        System.out.println("11.- Salir                                                                         *");
        System.out.println("************************************************************************************");
	}
}
