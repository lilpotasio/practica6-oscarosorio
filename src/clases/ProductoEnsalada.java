package clases;


public class ProductoEnsalada extends Producto{
	//atributos
	String tipoCarne;
	String sal;
	String pimienta;
	String salsaEnsalada;
	//constructor
	public ProductoEnsalada(String nPedido, String tipoCarne) {
		super(nPedido);
		this.tipoCarne = tipoCarne;
	}
	//getter and setter
	public String getTipoCarne() {
		return tipoCarne;
	}
	public void setTipoCarne(String tipoCarne) {
		this.tipoCarne = tipoCarne;
	}
	public String getSal() {
		return sal;
	}
	public void setSal(String sal) {
		this.sal = sal;
	}
	public String getPimienta() {
		return pimienta;
	}
	public void setPimienta(String pimienta) {
		this.pimienta = pimienta;
	}
	public String getSalsaEnsalada() {
		return salsaEnsalada;
	}
	public void setSalsaEnsalada(String salsaEnsalada) {
		this.salsaEnsalada = salsaEnsalada;
	}
	//to string
	@Override
	public String toString() {
		return "ProductoEnsalada [tipoCarne=" + tipoCarne + ", sal=" + sal + ", pimienta=" + pimienta
				+ ", salsaEnsalada=" + salsaEnsalada + ", toString()=" + super.toString() + "]";
	}
	//metodo propio de la subclase
	public void menuEnsaladaEspecial(int dia) {
		if (dia<15&&dia>=1) {
			System.out.println("ENSALADA ESPECIAL SUAVE Y LIGERA");
			System.out.println("Ensalada de pollo con una salsa suave para tu ensalada");
		} else {
			if (dia>=15&&dia<=31) {
				System.out.println("ENSALADA ESPECIAL PICANETE ENDEMONIADO");
				System.out.println("Esnasalda de ternera con salsa picante y pimienta");
				System.out.println("SOLO PARA LOS MAS ATREVIDOS");
				System.out.println("                              .\\\r\n" + 
						"                        .\\   / _\\   .\\\r\n" + 
						"                       /_ \\   ||   / _\\\r\n" + 
						"                        ||    ||    ||\r\n" + 
						"                 ; ,     \\`.__||__.'/\r\n" + 
						"         |\\     /( ;\\_.;  `./|  __.'\r\n" + 
						"         ' `.  _|_\\/_;-'_ .' '||\r\n" + 
						"          \\ _/`       `.-\\_ / ||      _\r\n" + 
						"      , _ _`; ,--.   ,--. ;'_ _|,     |\r\n" + 
						"      '`''\\| /  ,-\\ | _,-\\ |/''`'  _  |\r\n" + 
						"       \\ .-- \\__\\_/ /` )_/ --. /   |  |       _\r\n" + 
						"       /    .         -'  .    \\ --|--|--.  .' \\\r\n" + 
						"      |     /             \\     |  |  |   \\ |---'\r\n" + 
						"   .   .  -' `-..____...-' `-  .   |  |    |\\  _\r\n" + 
						".'`'.__ `._      `-..-''    _.'|   |  | _  | `-'      _\r\n" + 
						" \\ .--.`.  `-..__    _,..-'   L|   |    |             |\r\n" + 
						"  '    \\ \\      _,| |,_      /_7)  |    |   _       _ |  _\r\n" + 
						"        \\ \\    /       \\ _.-'/||        | .' \\     _| |  |\r\n" + 
						"         \\ \\  /.'|   |`.__.'` ||     .--| |--- _   /| |  |\r\n" + 
						"          \\ `//_/     \\       ||    /   | \\  _ \\  / | |  |\r\n" + 
						"           `/ \\|       |      ||   |    |  `-'  \\/  | '--|      _\r\n" + 
						"            `\"`'.  _  .'      ||    `--'|                |   .--/\r\n" + 
						"                 \\ | /        ||                         '--'\r\n" + 
						"                  |'|  mx     'J        made me do it! ;)\r\n" + 
						"               .-.|||.-.\r\n" + 
						"              '----\"----'");
				
			} else {
				System.out.println("dia incorrecto");
			}
		}
	}
}
