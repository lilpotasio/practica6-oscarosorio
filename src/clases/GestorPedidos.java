package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorPedidos {
	//variables
	int random;
	int acumulador;
	//creo los array list
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Producto> listaProductos;
	private ArrayList<ProductoEnsalada> listaProductosEnsalada;
	private ArrayList<ProductoKebab> listaProductosKebab;
	//creo constructor
	public GestorPedidos() {
		listaClientes = new ArrayList<Cliente>();
		listaProductos = new ArrayList<Producto>();
		listaProductosEnsalada = new ArrayList<ProductoEnsalada>();
		listaProductosKebab = new ArrayList<ProductoKebab>();
	}
	
	public void altaCliente(String nombre,String nMovil,String calle,String numero) {
		Cliente nuevoCliente = new Cliente(nombre, nMovil);
		nuevoCliente.setFechaAlta(LocalDate.now());
		nuevoCliente.setCalle(calle);
		nuevoCliente.setNumero(numero);
		listaClientes.add(nuevoCliente);
	}
	public void altaProducto(String nPedido,String tamano,String refrescoLata,String patatas) {
		Producto nuevoProducto = new Producto(nPedido);
		nuevoProducto.setPatatas(patatas);
		nuevoProducto.setRefrescoLata(refrescoLata);
		nuevoProducto.setTamano(tamano);
		listaProductos.add(nuevoProducto);
	}
	public void altaProductoKebab(String nPedido,String tamano,String refrescoLata,String patatas,String salsaBlanca,String tipoCarne,String queso,String salsaPicante) {
		ProductoKebab nuevoProductoKebab = new ProductoKebab(nPedido, tipoCarne);
		nuevoProductoKebab.setPatatas(patatas);
		nuevoProductoKebab.setQueso(queso);
		nuevoProductoKebab.setRefrescoLata(refrescoLata);
		nuevoProductoKebab.setSalsaBlanca(salsaBlanca);
		nuevoProductoKebab.setSalsaPicante(salsaPicante);
		nuevoProductoKebab.setTamano(tamano);
		listaProductosKebab.add(nuevoProductoKebab);
	}
	public void altaProductoEnsalada(String nPedido,String tamano,String refrescoLata,String patatas,String salsaBlanca,String tipoCarne,String sal,String pimienta,String salsaEnsalada) {
		ProductoEnsalada nuevoProductoEnsalada =new ProductoEnsalada(nPedido, tipoCarne);
		nuevoProductoEnsalada.setPatatas(patatas);
		nuevoProductoEnsalada.setRefrescoLata(refrescoLata);
		nuevoProductoEnsalada.setTamano(tamano);
		nuevoProductoEnsalada.setPimienta(pimienta);
		nuevoProductoEnsalada.setSal(sal);
		nuevoProductoEnsalada.setSalsaEnsalada(salsaEnsalada);
		listaProductosEnsalada.add(nuevoProductoEnsalada);		
	}
	public void listarCliente() {
		for (Cliente cliente : listaClientes) {
			if (cliente != null) {
				System.out.println(cliente);
			}
		}
	}
	public void listarProducto() {
		for (Producto producto : listaProductos) {
			if (producto!=null) {
				System.out.println(producto);
			}
		}
	}	
	public void listarProductoKebab() {
		for (ProductoKebab productoKebab : listaProductosKebab) {
			if (productoKebab!=null) {
				System.out.println(productoKebab);
				
			}
			
		}
	}
	public void listarProductoEnsalada() {
		for (ProductoEnsalada productoEnsalada : listaProductosEnsalada) {
			if (productoEnsalada != null) {
				System.out.println(productoEnsalada);
			}
		}
	}
	public Cliente buscarCliente(String nMovil) {
		for (Cliente cliente : listaClientes) {
			if (cliente!=null&&cliente.getnMovil().equals(nMovil)) {
				return cliente;
			}
		}
		return null;
	}
	public Producto buscarProducto(String nPedido) {
		for(Producto producto :listaProductos) {
			if (producto!=null&&producto.getnPedido().equals(nPedido)) {
				return producto;
			}
		}
		return null;
	}
	public ProductoKebab buscarProductoKebab(String nPedido) {
		for(ProductoKebab productoKebab : listaProductosKebab) {
			if (productoKebab!=null&&productoKebab.getnPedido().equals(nPedido)) {
				return productoKebab;
			}
		}
		return null;
	}
	public ProductoEnsalada buscarProductoEnsalada(String nPedido) {
		for (ProductoEnsalada productoEnsalada :listaProductosEnsalada) {
			if (productoEnsalada!=null&&productoEnsalada.getnPedido().equals(nPedido)) {
				return productoEnsalada;
			}
		}
		return null;
	}
	public void eliminarCliente(String nMovil) {
		Iterator<Cliente> iteradorClientes = listaClientes.iterator();
		while (iteradorClientes.hasNext()) {
			Cliente cliente = iteradorClientes.next();
			if (cliente.getnMovil().equals(nMovil)) {
				iteradorClientes.remove();
			}
			
		}
	}
	public void eliminarProducto (String nPedido) {
		Iterator<Producto> iteradorProductos = listaProductos.iterator();
		while(iteradorProductos.hasNext()) {
			Producto producto = iteradorProductos.next();
			if (producto.getnPedido().equals(nPedido)) {
				iteradorProductos.remove();
			}
		}
	}
	public void eliminarProductoKebab(String nPedido) {
		Iterator<ProductoKebab> iteradorProductosKebab = listaProductosKebab.iterator();
		while(iteradorProductosKebab.hasNext()) {
			ProductoKebab productoKebab = iteradorProductosKebab.next();
			if (productoKebab.getnPedido().equals(nPedido)) {
				iteradorProductosKebab.remove();
			}
		}
	}
	public void eliminarProductoEnsalada(String nPedido) {
		Iterator<ProductoEnsalada> iteradorProductosEnsalada = listaProductosEnsalada.iterator();
		while(iteradorProductosEnsalada.hasNext()) {
			ProductoEnsalada productoEnsalada = iteradorProductosEnsalada.next();
			if (productoEnsalada.getnPedido().equals(nPedido)) {
				iteradorProductosEnsalada.remove();
			}
		}
	}
	//metodo extra el cual son dos de azar para ganar algo, el primero genera un numero aleatorio y cada numero
	//es un premio y el segundo genera 3 n�meros aleatorios y si la suma de llos es mayor a cierto numero gana
	//el premio y si no nada
	public void juego(int juego) {
		random=(int)(Math. random()*10+1);
		switch (juego) {
		case 1:
			switch (random) {
			case 1:
				System.out.println("FELICIDADES!!! TIENES UN KEBAB DE TERNERA GRATIS");
				break;
			case 2:
				System.out.println("FELICIDADES!!! TIENES UNAS PATATAS GRATIS");
				break;
			case 3:
				System.out.println("FELICIDADES!!! TIENES UN REFRESCO GRATIS");
				break;
			case 4:
				System.out.println("FELICIDADES!!! TIENES UNA ENSALADA GRATIS");
				break;
			case 5:
				System.out.println("FELICIDADES!!! TIENES UN KEBAB DE POLLO GRATIS");
				break;
			case 6:
				System.out.println("FELICIDADES!!! TIENES UN KEBAB MIXTO GRATIS");
				break;
			case 7:
				System.out.println("FELICIDADES!!! TIENES UN KEBAB DE LO QUE QUIERAS GRATIS");
				break;
			default:
				System.out.println("FELICIDADES!!! TIENES UN DESCUENTO DE 20%");
				break;
			}
			break;
		case 2:
			acumulador=0;
			for (int i = 0; i < 3; i++) {
				random=(int)(Math. random()*10+1);
				acumulador=acumulador+random;
			}
			if (acumulador>=17) {
				System.out.println("FELICIDADES HAS GANADO UN MENU XL GRATIS");
			} else {
				System.out.println("Lo sentimos pero la pr�xima ser�");
			}
		default:
			System.out.println("Opcion selccionada no valida");
			break;
		}
		

	}
	//metodo extra ensalada
	public void menuEspecialEnsalada(ProductoEnsalada miEnsalada, int dia) {
		miEnsalada.menuEnsaladaEspecial(dia);
	}
	//metodo extra kebab
	public void menuEspecialKebab(ProductoKebab miKebab, int dia) {
		miKebab.menuKebabEspecial(dia);
	}
}	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
