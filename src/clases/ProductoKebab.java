package clases;

public class ProductoKebab extends Producto{
	//atributos
	String tipoCarne;
	String queso;
	String salsaBlanca;
	String salsaPicante;
	//constructor
	public ProductoKebab(String nPedido, String tipoCarne) {
		super(nPedido);
		this.tipoCarne = tipoCarne;
	}
	//setter and getter
	public String getTipoCarne() {
		return tipoCarne;
	}
	public void setTipoCarne(String tipoCarne) {
		this.tipoCarne = tipoCarne;
	}
	public String getQueso() {
		return queso;
	}
	public void setQueso(String queso) {
		this.queso = queso;
	}
	public String getSalsaBlanca() {
		return salsaBlanca;
	}
	public void setSalsaBlanca(String salsaBlanca) {
		this.salsaBlanca = salsaBlanca;
	}
	public String getSalsaPicante() {
		return salsaPicante;
	}
	public void setSalsaPicante(String salsaPicante) {
		this.salsaPicante = salsaPicante;
	}
	//to string
	@Override
	public String toString() {
		return "ProductoKebab [tipoCarne=" + tipoCarne + ", queso=" + queso + ", salsaBlanca=" + salsaBlanca
				+ ", salsaPicante=" + salsaPicante + ", nPedido=" + nPedido + ", tamano=" + tamano + ", refrescoLata="
				+ refrescoLata + ", patatas=" + patatas + "]";
	}
	//metodo especial de subclase producto kebab
	public void menuKebabEspecial(int dia) {
		if (dia<15&&dia>=1) {
			System.out.println("KEBAB ESPECIAL LIGHT");
			System.out.println("Kebab de pollo con salsa de yogur");
		} else {
			if (dia>=15&&dia<=31) {
				System.out.println("KEBAB ESPECIAL PICANETE ENDEMONIADO");
				System.out.println("Kebab de ternera con salsa picante como el infierno y pimienta");
				System.out.println("SOLO PARA LOS MAS ATREVIDOS");
				System.out.println("                              .\\\r\n" + 
						"                        .\\   / _\\   .\\\r\n" + 
						"                       /_ \\   ||   / _\\\r\n" + 
						"                        ||    ||    ||\r\n" + 
						"                 ; ,     \\`.__||__.'/\r\n" + 
						"         |\\     /( ;\\_.;  `./|  __.'\r\n" + 
						"         ' `.  _|_\\/_;-'_ .' '||\r\n" + 
						"          \\ _/`       `.-\\_ / ||      _\r\n" + 
						"      , _ _`; ,--.   ,--. ;'_ _|,     |\r\n" + 
						"      '`''\\| /  ,-\\ | _,-\\ |/''`'  _  |\r\n" + 
						"       \\ .-- \\__\\_/ /` )_/ --. /   |  |       _\r\n" + 
						"       /    .         -'  .    \\ --|--|--.  .' \\\r\n" + 
						"      |     /             \\     |  |  |   \\ |---'\r\n" + 
						"   .   .  -' `-..____...-' `-  .   |  |    |\\  _\r\n" + 
						".'`'.__ `._      `-..-''    _.'|   |  | _  | `-'      _\r\n" + 
						" \\ .--.`.  `-..__    _,..-'   L|   |    |             |\r\n" + 
						"  '    \\ \\      _,| |,_      /_7)  |    |   _       _ |  _\r\n" + 
						"        \\ \\    /       \\ _.-'/||        | .' \\     _| |  |\r\n" + 
						"         \\ \\  /.'|   |`.__.'` ||     .--| |--- _   /| |  |\r\n" + 
						"          \\ `//_/     \\       ||    /   | \\  _ \\  / | |  |\r\n" + 
						"           `/ \\|       |      ||   |    |  `-'  \\/  | '--|      _\r\n" + 
						"            `\"`'.  _  .'      ||    `--'|                |   .--/\r\n" + 
						"                 \\ | /        ||                         '--'\r\n" + 
						"                  |'|  mx     'J        made me do it! ;)\r\n" + 
						"               .-.|||.-.\r\n" + 
						"              '----\"----'");
				
			} else {
				System.out.println("dia incorrecto");
			}
		}
	}
	
}
