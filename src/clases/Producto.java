package clases;

public class Producto {

	//atributos
	String nPedido;
	String tamano;
	String refrescoLata;
	String patatas;
	Cliente clienteProducto;
	//constructor
	public Producto(String nPedido) {
		this.nPedido = nPedido;
	}

	//setter y getter


	public String getnPedido() {
		return nPedido;
	}

	public Cliente getClienteProducto() {
		return clienteProducto;
	}

	public void setClienteProducto(Cliente clienteProducto) {
		this.clienteProducto = clienteProducto;
	}

	public void setnPedido(String nPedido) {
		this.nPedido = nPedido;
	}

	public String getTamano() {
		return tamano;
	}

	public void setTamano(String tamano) {
		this.tamano = tamano;
	}

	public String getRefrescoLata() {
		return refrescoLata;
	}

	public void setRefrescoLata(String refrescoLata) {
		this.refrescoLata = refrescoLata;
	}

	public String getPatatas() {
		return patatas;
	}

	public void setPatatas(String patatas) {
		this.patatas = patatas;
	}
	//to string

	@Override
	public String toString() {
		return "Producto [nPedido=" + nPedido + ", tamano=" + tamano + ", refrescoLata=" + refrescoLata + ", patatas="
				+ patatas + ", clienteProducto=" + clienteProducto + "]";
	}

	
	
}
